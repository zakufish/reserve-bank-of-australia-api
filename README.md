# rba-api: Reserve Bank of Australia API
An unoffcial application programming interface for Reserve Bank of Australia.

Please be notified: This is not an official API, any change in RBA's front-end could lead to the possible crack-down of this interface.

Current version: 0.1
### Require
 --- Python 2.7

 --- pip

 --- demjson

 --- requests

### Current Features
1. Get cash rate.
2. Check the currency (Requesting dates limitations: 3 days from the latest date).
3. Inflation calculator.

### Install
Build package:
```
bash requires.sh
python setup.py build
python setup.py install
```

### Instructions

```
>>> import rba
>>> rba.getExchangeRate('USD', '27 Apr 2016')
0.7628
>>> rba.inflationCalculator(100, 1968, 2015)
1176.78
>>> rba.getCashRate('6 Apr 2016')
u'[{"ChangeInCashRate":"0.0","NewCashRateTarget":"2.00"}]'
```
