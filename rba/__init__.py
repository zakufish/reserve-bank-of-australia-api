#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Just a simple web crawler based API for RBA
"""
__author__      = "Ricky Yu"
__copyright__   = "Copyright 2016, MIT Lisence"

import re
import urllib2
import demjson
import requests


def getCashRate(date):
    """
    Date should be in the form of 'dd mm yyyy'
    """
    if type(date) is str:
        url = 'http://www.rba.gov.au/statistics/cash-rate/'
        content = urllib2.urlopen(url).read()
        pattern = '<th>' + date + '</th>\r\n\t\t\t\t\t\t\t\t\t<td>.*</td>\r\n\t\t\t\t\t\t\t\t\t<td>.*</td>'
        cash_rate = re.findall(pattern, content)[0]
        change_in_cash_rate = cash_rate.replace('<th>' + date + '</th>\r\n\t\t\t\t\t\t\t\t\t<td>', '').replace(
            "</td>\r\n\t\t\t\t\t\t\t\t\t<td>.*</td>", '')
        change_in_cash_rate = getReturn(change_in_cash_rate)
        new_cash_rate_target = cash_rate.replace('<th>' + date +
                                                 '</th>\r\n\t\t\t\t\t\t\t\t\t<td>0.00</td>\r\n\t\t\t\t\t\t\t\t\t<td>',
                                                 '').replace('</td>', '')
        encoding_data = [{'ChangeInCashRate': str(change_in_cash_rate), 'NewCashRateTarget': new_cash_rate_target}]
        data = demjson.encode(encoding_data)
        return data
    else:
        print 'Date type is not valid. Please check.'
        return


def getExchangeRate(currency, date):
    currency_dict = {'USD': 'United States dollar',
                     'CNY': 'Chinese renminbi',
                     'JPY': 'Japanese yen',
                     'EUR': 'European euro',
                     'KRW': 'South Korean won',
                     'SGD': 'Singapore dollar',
                     'NZD': 'New Zealand dollar',
                     'GBP': 'UK pound sterling',
                     'MYR': 'Malaysian ringgit',
                     'THB': 'Thai baht',
                     'IDR': 'Indonesian rupiah',
                     'INR': 'Indian rupee',
                     'TWD': 'New Taiwan dollar',
                     'VND': 'Vietnamese dong',
                     'HKD': 'Hong Kong dollar',
                     'PGK': 'Papua New Guinea kina',
                     'CHF': 'Swiss franc',
                     'AED': 'United Arab Emirates dirham',
                     'CAD': 'United Arab Emirates dirham',
                     'TWI_4pm': 'Trade-weighted Index (4pm)',
                     'SDR': 'Special Drawing Right'
                     }
    if type(currency) is str and type(date) is str:
        search_pattern = currency_dict[currency]
        url = 'http://www.rba.gov.au/statistics/frequency/exchange-rates.html'
        info = urllib2.urlopen(url).read()
        day_ref = re.findall('<strong>.*</strong>', info)[0]
        day_ref = day_ref.replace('<strong>', '').replace('</strong>', '')
        pos = int(day_ref.split(' ')[0]) - int(date.split(' ')[0])
        reg = re.findall(
            '<td class="rowBg">' + search_pattern +
            '</td><td>.*</td><td>.*</td><td style="background-color:#f6f9fc;">.*</td>',
            info)[0]
        if pos == 2:
            reg = reg.replace('<td class="rowBg">' + search_pattern + '</td><td>', '')
            return getReturn(reg)
        elif pos == 1:
            reg = reg.replace('<td class="rowBg">' + search_pattern + '</td><td>', '')
            reg = re.findall('</td><td>.*</td>', reg)[0].strip('</td><td>')
            return getReturn(reg)
        elif pos == 0:
            reg = re.findall('<td style="background-color:#f6f9fc;">.*</td>', reg)[0].strip(
                '<td style="background-color:#f6f9fc;">')
            return getReturn(reg)
        else:
            print 'Invalid date or currency input'
            return

def getReturn(regex):
    num = ''
    for element in regex:
        if element != '<':
            num += element
        else:
            break
    return float(num)

def inflationCalculator(annual_dollar, annual_start_year, annual_end_year):
    params = {'annualDollar': annual_dollar, 'annualStartYear': annual_start_year, 'annualEndYear': annual_end_year}
    r = requests.post('http://www.rba.gov.au/calculator/annualDecimal.html', data=params)
    post_info = r.text

    calculated_annual_dollar_value = re.findall('name="calculatedAnnualDollarValue"\r\n\t\t\tvalue=".*" style="', post_info)[0]
    calculated_annual_dollar_value = calculated_annual_dollar_value.replace('" style="', '').replace(
        'name="calculatedAnnualDollarValue"\r\n\t\t\tvalue="', '')
    return float(str(calculated_annual_dollar_value.replace(',', '')))
