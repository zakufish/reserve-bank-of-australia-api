from distutils.core import setup
setup(
  name='rba',
  packages=['rba'],
  version='0.1',
  description='An unofficial application programming interface for Reserve Bank of Australia',
  author='Ricky Yu',
  author_email='lijierickyyu@gmail.com',
  url='https://github.com/zakufish/rba-api',
  keywords=['reserve-bank-of-australia', 'api', 'currency', 'cpi']
)
